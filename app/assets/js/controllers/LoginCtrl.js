app.controller('LoginCtrl', function($scope, LoginService){
  // settings
  $scope.user = {};
  $scope.isLoading = false;
  $scope.feedback = '';

  // methods
  $scope.doLogin = function (form) {
    $scope.feedback = '';

    if (form.$valid) {
      $scope.isLoading = true;
      LoginService.doLogin($scope.user).then(
          function success(res) {
            $scope.isLoading = false;
            window.location.href = '#/dashboard';
          },
          function error(err) {
            $scope.isLoading = false;
            $scope.feedback = err;
          }
      );
    }
  };
});

app.service("LoginService", function($q) {
  var self = {
    'ref' : new Firebase('https://promo-nncl.firebaseio.com'),
    'doLogin' : function (user) {
      var d = $q.defer();

      self.ref.authWithPassword({
        email    : user.email,
        password : user.password
      }, function(error, authData) {
        if (error) {
          console.log("Login Failed!", error);
          d.reject(error);
        } else {
          console.log("Authenticated successfully with payload:", authData);
          d.resolve(authData);
        }
      });

      return d.promise;
    }
  };

  return self;
});
