app.controller('AppCtrl', function($scope, $firebaseArray, Items){

  // settings
  $scope.items = Items;
  $scope.user = {};
  $scope.single = [];
  var ref = new Firebase('https://promo-nncl.firebaseio.com');

  $scope.getData = function () {
    $scope.items = $firebaseArray(ref.child('items'));
  };

  $scope.getUser = function (user) {
    var id = user.$id;
    var items = ref.child('proposts');
    $scope.single = $firebaseArray(items.child(id));
  };

});

app.service("Items", function($firebaseArray) {
  var itemsRef = new Firebase("https://promo-nncl.firebaseio.com/items");
  return $firebaseArray(itemsRef);
});
