app.controller('RegisterCtrl', function($scope, $firebaseArray, Items){

  // settings
  $scope.user = {};
  var items = Items;

  $scope.send = function (form) {
    console.log('AppCtrl::send');
    if (form.$valid) {
      var propost = $scope.user;

      items.$add({
        'propost' : propost
      }).then(function (data) {
        reset(form);
      })
    }
  };

  var reset = function (form) {
    $scope.user = {};
    form.$setPristine();
    alert('Cadastrado com sucesso. Boa sorte :)')
  }

});

app.service("Items", function($firebaseArray) {
  var itemsRef = new Firebase("https://promo-nncl.firebaseio.com/proposts");
  return $firebaseArray(itemsRef);
});
